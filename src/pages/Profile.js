import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
    Container,
    Header,
    Content,
    Footer,
    FooterTab,
    Button,
    Icon,
    Left,
    Right,
    Body,
    Title
  } from "native-base";
  
export default class Profile extends React.Component {

  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Icon
        name='user-o'
        type="FontAwesome"
        size={50}
        style={[{ color: tintColor, fontSize: 25}]}
      />
    )
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Profile!!!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});