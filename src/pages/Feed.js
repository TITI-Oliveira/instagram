import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Title
} from "native-base";
import { Font, AppLoading } from "expo";

export default class Feed extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: false
    };
  }
  async componentDidMount() {
    (async () => {
      await Font.loadAsync({
        Billabong: require("../assets/fonts/Billabong.ttf"),

      });

      this.setState({ isReady: true });


    })();
  }
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Icon
        name={'home'}
        type='MaterialCommunityIcons'
        size={50}
        style={[{ color: tintColor }]}
      />
    )
  };

  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    return (
      <View>
          <Header
            style={{
              backgroundColor: "#fdfdfd",
              borderBottomColor: "#dadada",
              borderBottomWidth: 1,
              height: 70
            }}
          >
            <Left>
              <Icon
                name={'camera'}
                type='SimpleLineIcons'
                size={50}
                style={[{ color: '#000', marginLeft: 5, fontSize: 25 }]}
              />
            </Left>
            <Body>
              <Title
                style={{ fontFamily: 'Billabong', fontSize: 35, color: "#000", marginLeft: 5 }}
              >
                instagram
                </Title>
            </Body>
            <Right>
              <Icon
                name={'paper-plane'}
                type='SimpleLineIcons'
                size={50}
                style={[{ color: '#000', marginRight: 5, fontSize: 25 }]}
              />
            </Right>
          </Header>


      <View style={{height: 90, backgroundColor: '#fcfcfc', borderBottomColor: '#eaeaea', borderBottomWidth: 1}}>
            <View style={{flexDirection: "row"}}>
            </View>
      </View>



        </View>
        );
      }
    }
    
const styles = StyleSheet.create({
          container: {
          flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
    });
