import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity
} from "react-native";
import {
  Container
} from "native-base";
import { createBottomTabNavigator } from "react-navigation";
import AddPhoto from "./AddPhoto";
import Explore from "./Explore";
import Feed from './Feed';
import LikesPage from "./LikesPage";
import Profile from "./Profile";

export default class TabRouter extends React.Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <Container>
        <AppTabNavigator screenProps={this.props.navigation}/>
      </Container>
    );
  }
}

const AppTabNavigator = createBottomTabNavigator({
  Feed: {
    screen: Feed
  },
  Explore: {
    screen: Explore
  },
  AddPhoto: {
    screen: AddPhoto
  },
  LikesPage: {
    screen: LikesPage
  },
  Profile: {
    screen: Profile
  }
},
{
  animationEnabled: true,
  swipeEnabled: false,
  tabBarPosition: "bottom",
  tabBarOptions:{
    style:{
      height: 50,
      backgroundColor: '#fff'
    },
    activeTintColor: '#000',
    inactiveTintColor : '#bababa',
    showLabel: false,
    showIcon: true


  }


}
)


