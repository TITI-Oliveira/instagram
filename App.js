import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import TabRouters from './src/pages/TabRouters'


export default createStackNavigator({
  'TabRouters': {
    screen: TabRouters,
    headerMode: 'none',
    navigationOptions:{
      headerVisible: false,
      header: null, 
      gesturesEnabled: false
    },
  },
})
